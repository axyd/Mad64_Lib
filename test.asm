;
; This file is used to test procedures
;

;prototypes, required to use external procedure

ExitProcess PROTO
m64print	PROTO
m64nl		PROTO
m64nlx		PROTO
m64clr		PROTO
m64beep		PROTO
m64regs		PROTO
m64slen		PROTO
m64prompt	PROTO
m64scan		PROTO


.data
ifmt		byte "%d",0
sfmt		byte "%s",0
ffmt		byte "%f",0
msgHello	byte "Hello",0
msgGetNr	byte "Enter a number: ",0
msgGetStr	byte "Enter a string: ",0
msgDispNr	byte "You entered: %d",0
arr			qword 51, 37, 56, 89, 0, 99
;data storage variables
intStor	qword 0
strStor byte 80 DUP(?)
fltStor REAL8 0.0

.code

Main PROC
	mov rcx, offset sfmt
	mov rdx, offset msgHello
	call m64print

	;display empty lines
	call m64nl
	call m64nl

	;clear console
	;call m64clr
	
	;make a sound
	mov rcx, 75
	mov rdx, 500
	call m64beep

	call m64regs

	mov rbx, 5
	call m64nlx

	;string length
	mov rdx, offset msgHello
	call m64slen
	mov rcx, offset ifmt
	mov rdx, rax
	call m64print
	call m64nl
	
	;array size, use LENGTHOF operator
	mov rcx, offset ifmt
	mov rdx, LENGTHOF arr
	call m64print
	call m64nl

	;prompt message and get data
		;integer
	mov rbx, offset intStor
	mov rcx, offset ifmt
	mov rdx, offset msgGetNr
	call m64prompt

	mov rcx, offset ifmt
	mov rdx, [intStor]
	call m64print
	call m64nl

		;string
		;NOTE: string has to be terminated by a ! char
	mov rbx, offset strStor
	mov rcx, offset sfmt
	mov rdx, offset msgGetStr
	call m64prompt

	mov rcx, offset sfmt
	mov rdx, offset strStor
	call m64print
	call m64nl

;TEST m64scan
	;message
	mov rcx, offset sfmt
	mov rdx, offset msgGetNr
	call m64print
	
	;scan data
	mov rcx, offset ffmt
	mov rdx, offset fltStor
	call m64scan

	;display
	mov rcx, offset ffmt
	mov rdx, offset fltStor
	call m64print
	call m64nl

	
	call ExitProcess
Main ENDP
END
