comment !

Author: Jose Madureira
Date: 20171026

#######################################
			PROCEDURE STATUS
#######################################

**************
** Complete	**
**************
	m64print	- console output, user defined
	m64nl		- console output, empty line
	m64nlx		- console output, multiple empty lines
	m64clr		- clear console
	m64beep		- makes a sound, user defined
	m64regs		- displays register HEX values
	m64slen		- returns string length

*************
** Partial **
*************
	m64prompt	- display message, receive data
		MISSING: DATA FORMATS OTHER THAN INT/STR
	m64scan		- read data input
		NOT PROCESSING FLOATS

******************
** Not complete	**
******************
	scanf integer, double, float, string, char, hex
	message box: close, ok/cancel
	timer
	prompt (out msg, in receive)
	show RFLAGS
	show FPU stack
	mt19937 randomizer
	get time: epoch
	idle for n seconds
	deg > rad
	rad > deg
   	string copy
	file download
	file: read, write, close
!

COMMENT !
		###### GENERAL NOTES ######

 # REGISTER USAGE BY FUNCTION CALLS CALLS#
	Volatile Registers:		RAX,RCX,RDX,R8-R11 
	Non-volatile Registers: RBX,RDI,RSI,RBP,RSP,R12-R15

 # ARGUMENT PASSING ORDER
	|  1  |  2  |  3  |  4  |  5+   |
	|-----|-----|-----|-----|-------|
	| RCX | RDX |  R8 |  R9 | STACK |

 # Array size: mov <reg>, LENGTHOF <variable name>

!

; .: Global constants :.
STR_MAX_SZ	EQU 50			;Max string buffer size
TAB			EQU 09h			;Tab
NL			EQU 0Dh,0Ah		;New line
TYP			EQU TYPE RAX	;64 bit Type

; .: External Libraries :.
includelib msvcrt.lib		;console API
includelib kernel32.lib		;standard windows API

; .: External Procedures :.
EXTERN printf:PROC
EXTERN scanf:PROC
EXTERN Beep:PROC
EXTERN Sleep:PROC
EXTERN ExitProcess:PROC		;use to end program
EXTERN system:PROC			;clear console
EXTERN putchar:PROC			;output single char
EXTERN getchar:PROC			;receives single char
EXTERN strcmp:PROC			;compares string
EXTERN scanf:PROC			;read string


.data
;printf string formats
fmtStr byte "%s",0
fmtInt byte "%d",0
fmtHex byte "%X",0
fmtFlt byte "%f",0
fmtReg byte "%3s: %X",0		;m64regs
fmtBin byte "%b",0
getstr byte "%[^!]",0		;read chars until !

;strings:
	;m64clr
syscls byte "cls",0
	;m64regs
regsStr byte "RAX",0, "RBX",0, "RCX",0, "RDX",0, "RBP",0
		byte "RSP",0, "RSI",0, "RDI",0, "R8 ",0, "R9 ",0
	    byte "R10",0, "R11",0, "R12",0, "R13",0, "R14",0
		byte "R15",0
msgNoTM byte "No type match",0

;flags
flags qword 0
tblhex qword 0h, 1h, 2h, 3h, 4h, 5h, 6h, 7h, 8h, 9h
	   qword 10h, 11h, 12h, 13h, 14h, 15h
tblhex2bin byte 0


arr qword 50, 37, 56, 89, 0, 99

;temporary storage
tempi qword 0
temps byte 31 DUP(?)

.code

; *******************************************************
m64print PROC
; * Purpose: displays a formatted message				*
; *-----------------------------------------------------*
; *	IN : RCX - string format							*
; *		 RDX - pointer to data/string var				*
; *	OUT: none											*
; *******************************************************
	sub rsp,32		;reserve shadow space
	call printf		;display message
	add rsp,32		;clean up

	ret
m64print ENDP


; *******************************************************
m64nl PROC
; * Purpose: outputs empty line							*
; *-----------------------------------------------------*
; *	IN : none											*
; *	OUT: none											*
; *******************************************************
	mov rcx, 10		;"\n"

	sub rsp,32
	call putchar
	add rsp,32
	
	ret
m64nl ENDP

; *******************************************************
m64nlx PROC
; * Purpose: outputs 'x' number of empty lines 			*
; *-----------------------------------------------------*
; *	IN : RBX											*
; *	OUT: none											*
; *******************************************************
		
next:
	call m64nl
	dec rbx
	jnz next

	ret
m64nlx ENDP


; *******************************************************
m64clr PROC
; * Purpose: clears console								*
; *-----------------------------------------------------*
; *	IN : none											*
; *	OUT: none											*
; *******************************************************
	mov rcx, offset syscls	;"cls"

	sub rsp, 32
	call system
	add rsp, 32

	ret
m64clr ENDP


; *******************************************************
m64beep PROC
; * Purpose: clears console								*
; *-----------------------------------------------------*
; *	IN : RCX - frequency (hz) [not volatile]			*
;		 RDX - duration (ms)  [not volatile]			*
; *	OUT: none											*
; *******************************************************
	push rcx
	push rdx

	sub rsp, 32
	call Beep
	add rsp, 32

	pop rdx
	pop rcx
	ret
m64beep ENDP


; *******************************************************
m64regs PROC
; * Purpose: displays register HEX values				*
; *-----------------------------------------------------*
; *	IN : none											*
; *	OUT: none											*
; *******************************************************
	;store registers on the stack
	sub rsp, 128
	mov [rsp+120], r15
	mov [rsp+112], r14
	mov [rsp+104], r13
	mov [rsp+96],  r12
	mov [rsp+88],  r11
	mov [rsp+80],  r10
	mov [rsp+72],  r9
	mov [rsp+64],  r8
	mov [rsp+56],  rdi
	mov [rsp+48],  rsi
	mov [rsp+40],  rsp
	mov [rsp+32],  rbp
	mov [rsp+24],  rdx
	mov [rsp+16],  rcx
	mov [rsp+8],   rbx
	mov [rsp+0],   rax

	xor rbx, rbx			;loop counter
print:
	mov rcx, offset fmtReg
	lea rdx, [regsStr+(rbx*4)]	;register name
	mov r8, [rsp+(rbx*8)]		;register value
	call m64print
	call m64nl

	inc rbx					;next register
cmp	rbx,16
jb print

	add rsp,128				;clean the stack
	ret
m64regs ENDP



; *******************************************************
m64flags PROC
; * Purpose: displays 64-bit RFLAGS						*
; *-----------------------------------------------------*
; *	IN : none											*
; *	OUT: none											*
; *******************************************************
	;PUSHFQ			;store flags, mandatory step
	;POP flags		;retrieve flags
	
	;lea rbx, flags
	xor rax,rax
	PUSHFQ
	pop rdx

	xor r12, r12	;byte counter
	mov rdx, offset tblhex
eachbyte:
	mov rcx, offset fmtHex
	call m64print
	call m64nl
	
	add rdx, 8
	inc r12
cmp r12, 8
jb eachbyte

	
	ret
m64flags ENDP


; *******************************************************
m64slen PROC
; * Purpose: returns the length of a string		 		*
; *-----------------------------------------------------*
; *	IN : RDX - pointer to string						*
; *	OUT: RAX - length of string							*
; *******************************************************
	mov rax, -1
count:					;count characters until terminator
	inc rax
	cmp byte ptr[rdx+rax], 0h
	jne count	

	ret
m64slen ENDP


; *******************************************************
m64prompt PROC
; * Purpose: output message, receives data		 		*
; *			 detects data type							*
; *-----------------------------------------------------*
; *	IN : RCX - data format string						*
; *		 RDX - pointer to message						*
; *		 RBX - pointer to storage variable				*
; *	OUT: RDX - data										*
; *******************************************************
	;display message
	push rcx
	mov rcx, offset fmtStr
	call m64print
	pop rcx	
	
;detect the data type to receive
compare:		
	;integer
	push rcx
	mov rdx, offset fmtInt
	call strcmp		;compare rcx with rdx
	pop rcx
	cmp rax, 0
	je isint
	
	;string
	mov rdx, offset fmtstr
	call strcmp		;compare rcx with rdx
	cmp rax, 0
	je isstr

fail:				;no type match
	mov rcx, offset fmtStr
	mov rdx, offset msgNoTM
	call m64print
	jmp endex

isint:
	mov rcx, offset fmtInt
	mov rdx, offset tempi
	sub rsp, 32
	call scanf
	add rsp, 32

	mov rax, [tempi]
	mov [rbx], rax
	jmp endex	

isstr:
	sub rsp, 32
	mov rcx, offset getStr
	mov rdx, rbx
	call scanf		;read until ! char
	call getchar	;eat stray new line
	add rsp, 32
	jmp endex
endex:

	ret
m64prompt ENDP


; *******************************************************
m64scan PROC
; * Purpose: get data input						 		*
; *-----------------------------------------------------*
; *	IN : RCX - data format string						*
; *		 RDX - pointer to storage						*
; *	OUT: none											*
; *******************************************************
	push rcx
	push rdx
checkforstring:
	mov rdx, offset fmtstr
	sub rsp, 32
	call strcmp		;compare rcx with rdx
	add rsp, 32

	cmp rax, 0
	jne notstr		;input wont be a string

isstr:
	mov rcx, offset getStr
	pop rdx			;data format string
	pop rcx

	sub rsp, 32
	call scanf		;read until ! char
	call getchar	;eat stray new line
	add rsp, 32

	jmp done

notstr:
	pop rdx
	pop rcx

	sub rsp, 32
	call scanf
	add rsp, 32

done:

	ret
m64scan ENDP

END
